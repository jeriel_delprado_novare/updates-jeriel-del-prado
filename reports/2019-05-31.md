# 2019-05-31

## Weekly Summary

This week’s efforts focus more on Speedforce due to the ISG team building event on Thursday and Friday.

## Initiatives

**Jumpstart**

- In response to the previous week’s Mancom, the Compliance Sheet has been set up along with the necessary requirements that were sent to all Agile Coaches.
- The team has decided to approach the mandate on a *per-vendor* basis instead of micromanaging each individual project — we’ll reach out to each of the vendors in the next weeks to enforce the requirements on their projects.

**Process and Governance**

- Discussed matters with ISDP and Trend Micro - a workshop will be arranged on all concerned parties so that Globe’s developers will be able to utilize Trend Micro’s container scanning technology.

**Speedforce**

- Minor improvements to Speedforce's code. No new features, only bug fixes and general improvements.

**Jira**

- Attended meeting with Veron to discuss the deliverables that was postponed from the original schedule last week.
- So far, B2B efforts have calmed down — the team is currently learning how it all works and our team is on standby for any Jira concerns and administrative support.
