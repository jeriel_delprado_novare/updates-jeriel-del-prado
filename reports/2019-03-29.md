# 2019-03-29

## DevOps Software Factory

**Infrastructure + Speedforce**

- Team is preparing an nginx instance for routing devops.globe.com.ph request
- Feature implementation on pricing analysis is in development.
- Team is preparing Slack webhooks support for all future CodeCommit repositories added in Landing Zone.

**Jumpstart**

- Prepared Roadmap and Workflow for DevOps.
- Prepared invitation for software factories.
- Started preparing access to Connect team.
- Team has submitted scans and findings of the Globe One ES project.
- Team has prepared CI pipelines for the Globe One ES project.

## Administrative

Renz Villavito has declared an intent to resign from Novare. (2019-03-29).

