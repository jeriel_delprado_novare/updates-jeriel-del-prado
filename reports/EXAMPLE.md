; In this example, all lines starting with semicolons are comments.
; You're supposed to omit them when writing your own status update.
; 
; It's recommended to submit reports like these at the end of the working day,
; or first thing in the morning describing what happened yesterday.
;
; Start with a **title** of the current date, in `YYYY-MM-DD` format. 
; The filename should be formatted the same way with `YYYY-MM-DD.md`
;
; The entire document should be written in [Markdown](https://github.github.com/gfm/) so it's
; formatted nicely on plain text editors and in BitBucket.

# 2018-01-14

; For every project you're involved in, format it as a heading (e.g., `## Project Name`)
; My current project is the "DevOps Software Factory (Globe)", so write it like this.
;
; As a rule of thumb: label your projects as `PROJECT NAME (CLIENT)`. 


## DevOps Software Factory (Globe)

**Updates Today**

; Enter your report here. Keep your reports meaningful but short; if possible.
; Bullets are encouraged, but optional.
; A good way to think about these updates is writing tweets or version history updates. They keep their sentences to
; 160 characters max, or in a neat list that's easy to read.
;
; If you need to explain / elaborate further, consider placing more of it in a separate section at the end.

- Vacation just ended; currently catching up with the team and on emails.
- Attended meeting with Max at TGT on the DevOps Roadmap for the year.
- Responded emails on the AEM DevOps meeting invite slated this Wednesday; currently waiting for replies.
- Sent a follow-up email to Beeps and Max about the Security Pass requirements for the team.
- Submitted Agile Accomplishments to Richley and team.

**Callouts**

; Enter any important or urgent callout about your project.
; State "No issues" if there aren't any.

- No stand-up meetings were held lately due to contract issues bet. Novare and Globe. This is no longer a problem.
- Handling issues between DevOps and production are currently in talks for future meetings.

**Up Next**

; Write what you're expected to do for the next day.

- Examine the recently submitted repositories and see how the DevOps Jumpstart process will work for Globe One End State.
- Review all current team tasks and see which are actionable and which aren't.


## SEG Armory

**Updates Today**

- Added a new Severity (Number) field for Redmine projects.
- Reviewed Sonar and Jenkins.
- Talked with Dale about other SEG projects using the `seg-infra` AWS workspace.
- Added new tasks for Armorers to explore in Wekan.

**Callouts**

No issues.

**Up Next**

- Set up a meeting for Armory for upcoming tasks and member assignments.


## Administrative Tasks

**Updates Today**

- Currently reading up on KPIs and KRAs for team members to follow.

**Callouts**

No issues.

**Up Next**

- Set up 1-on-1 meetings with admin members.
- Review team members' evaluation forms; especially those who will be regularized in a few weeks.


