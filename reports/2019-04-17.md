# 2019-04-17

## DevOps Software Factory

- [x] [Meeting] Systems Team + IEM - Progress report, Collaboration with Landing Zone Prod + BB App (can be done with above) (Update: We’ve had a meeting, nothing actionable on our part, but meeting minutes are recorded.)
- [x] [Meeting] Service Transition + DevOps - Attended meeting with  @Gian ,  @Max  and  @Beeps  
- [x] [Task]  @Jeriel  Submit meeting minutes via email to all attendees, along with links to the DevOps @ Globe site.
- [x] [Task]  @Jeriel  Prepare a Google Form for collecting feedback.
- [x] [Task]  @Jeriel  Submit the Google Form to all attendees.
