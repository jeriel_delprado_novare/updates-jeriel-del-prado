# 2019-01-17

## DevOps Software Factory (Globe)

**Updates Today**

- Attended meetings regarding Amdocs + DevOps efforts at Globe.
- Updated the DevOps roadmap to include the findings from 
  yesterday's meeting.
  - Added phase zero requirements compliance.
  - Added list of Globe requirements needed for DevOps to proceed.
  - Updated the hosted roadmap page in Google Drive.

**Callouts**

No issues.

**Up Next**

- Ongoing reviews on code.
- Ongoing "Sprint 12".

## SEG Armory

**Updates Today**

- Wekan server has been updated to v2.00.

**Callouts**

No issues.

**Up Next**

- Set up a meeting for Armory for upcoming tasks and member assignments.


## Administrative Tasks

**Updates Today**

- Submitted admin review forms. 

**Callouts**

No issues.

**Up Next**

- Set up 1-on-1 meetings with admin members.
- Review team members' evaluation forms
