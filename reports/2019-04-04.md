# 2019-04-04

## DevOps Software Factory

**Process and Governance**

- Beeps has a request to prepare presentation decks for Business.
  - Describe the roadmap for Globe on DevOps for process transformation.
  - Details should describe: Agile, DevOps, Cloud, Containerization and
    all the processes in between!
  - Describe a long-term (5-6 yr.) timeline on where we should be at
    process transformation.
  - As discussed with @Beeps, @Gian and @Max, the "slides" and "roadmap" 
    for this effort *does not exist* and has to be prepared.
- Jesse has a request to prepare presentation decks about Support and 
  Post-production transformation.
  - Describe L1, L2 and L3 support and where they are now.
  - Illustrate the new operating model with DevOps in mind.

**Jumpstart**

- Met with @Benj to discuss about Emerging Tech
  - We need a list of projects under Emerging Tech.
  - Classify which projects should be out of pilot / incubation and which
    projects aren't and meant for production.
  - Emerging Tech gets to decide whether to pursue full DevOps or not
    since most projects are experimental.
  - For projects that WILL go live, these projects should undergo the
    DevOps process.
  - Emerging Tech is mostly *independent*, since it has its own budget
    for its AWS environment. "Production" and "Dev" environments are
    all maintained by Emerging Tech themselves.
- Also discussed Forge Praxis with @Benj
  - Forge Praxis follows DevOps compliance already.
  - Forge Praxis is in *production* already.
  - They'll provide design documents to complete the Jumpstart.
  - They'll need to submit their pipeline's work to follow Sonarqube, 
    Fortify and other DevOps tools in the future.

