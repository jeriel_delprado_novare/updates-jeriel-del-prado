# 2019-04-11

## DevOps Software Factory

- [x] [Task] @Jeriel - Archive all Solution Plans sent via Email
	- [x] Connect (Phase 1)
	- [x] Ripley (Diagrams)
	- [x] Globe One ES
- [x] [Task] @Jeriel - Fortify + Globe One: Responded to RR’s reply
- [x] [Task] @Jeriel - Jira + Connect: Fulfilled @Shey’s request on solution fields.
- [ ] [Meeting] @Max @Jeriel @Veron - Jira + Tools: Meeting on Confluence vs Google Sites
- [ ] [Task] @Jeriel - Beeps: Business-friendly roadmap for Agile, Cloud, DevOps and Containerization and in lieu with a Cloud Adoption Framework.

