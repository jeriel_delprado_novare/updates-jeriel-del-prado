# 2019-02-15

## DevOps Software Factory

The team is focusing on governance and onboarding processes.
- Attended AWS training on containers, ECS, EKS and Fargate setups.
- Created the DevOps @ Globe site, used to centralize all information about DevOps on our governance efforts.
- Met with TCOE, ISDP, IEM, Change Mgmt. and PMO.
- Speedforce resumes development.
- Updated reports on Globe One End State and submitted to Globe for review.
- Ongoing tests on Device Farm + Globe One End State.

## PPCRV

- The team has provided a lot of efforts to bring the PPCRV Donation Site up to speed.
  - Implemented the backend with PayMaya integration and database logging.
  - Implemented the frontend Angular setup.
  - Implemented the DevOps pipeline for both frontend and backend.
  - Added preparations for production (HTTPS certificates, Route53 domain and CloudFront)


## Administrative

- Submitted appraisal forms for all 4 admin members for this evaluation period.
- Still currently working on self-assessment.
- Still pending one on one with Dale.
