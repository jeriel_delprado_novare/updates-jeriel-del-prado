# 2019-04-12

## DevOps Software Factory

- [x] Compiled the status report for the week
- [x] Mostly brainstorming with existing todos (Beeps' request) and closing
      team tasks 

## Administrative

- [x] Followed-up Renz' early dismissal request to @Dale
- [x] Attended the TM meeting.

