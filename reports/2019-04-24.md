# 2019-04-24

## DevOps Software Factory

- [x] [Task] ESB - Pobs has committed the code, check and see if the code is present. (Update, it’s not)
- [x] [Task] Cast Highlight -  A scan had just occurred, send notice to Pia and Princes about what to expect. (Update: Message sent, we’re waiting for Sutrix now.)
- [x] [Task] Jira - Discuss Jira arrangements with Veron (Update: minutes recorded, team will assist and collaborate)
- [x] [Meeting] ISDP - Negotiate Fortify action items with RR and Beej. (Update: RR and Beej didn’t show up! Escalate this to Beeps) 
- [x] [Meeting] Beeps wants to meet with the entire team at TGT 16F today. (Update: Meeting minutes recorded, lots of tasks to do next)
- [x] [Follow-up] ISDP - Remind the team about our pending requests; not just Fortify.
- [x] [Follow-up] ITSMO - Send email follow-ups on the Feedback form you presented last Wednesday.
- [x] [Task] Formulate the plans based on Beeps meeting.
