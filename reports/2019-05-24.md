# 2019-05-24

Month-long gap since the previous entry! Here's why:

- Status updates have been moved to a **Notion-based Workflow**. That
  means my updates have been collaborative with the team and the actual
  documents are only processed by bulk.

- The individual updates themselves aren't as meaningful outside of the
  DevOps Software Factory updates, and repeating myself isn't exactly
  a productive use of my time.

- Weekly updates are also provided through Notion, and fed to the 
  project's Status Updates Google Doc.

So instead, I'll just list down what I've had lately that wasn't 
present in Notion.

## DevOps Software Factory

- Massive expectations coming from higher-ups. We're feeling the pressure
  of delivering CI/CD processes to so much Software Factories, it's 
  becoming very scary for everyone.

- Various progresses for new projects and fresh projects alike, such
  as Ripley 1/2/3, Dalmore, Go Roam, ESB, Community, Connect and 
  Forge Praxis.


## Administrative

- I got promoted, along with Keifer and Joseph for a job well done.
  The team enjoyed its team lunch with Beeps and Max at Romantic Baboy
  near GTP too.


