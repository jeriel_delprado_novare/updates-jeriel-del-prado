# 2019-03-01

## DevOps Software Factory

The team is partially handling issues between Brie and its BlackPink incident and Speedforce.
- Ongoing code review for the Brie incident; currently preparing material for the NPC audit next week.
- Ongoing preparations for the DevOps checkpoint with GIl next week.
- Team is currently implementing project metadata information on Speedforce.
- Finished Jumpstart with Globe One ES and Brie.

## PPCRV

Handled the `CNAME` routing and certificate handling for the donate.ppcrv.org domain.

## Administrative

Submitted signed personal appraisal form to Dale.
Reported concerns about regularization for the new admin members.
