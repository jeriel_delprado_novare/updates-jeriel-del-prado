# 2019-04-16

## DevOps Software Factory

- [x] [Meeting] Process Transformation plans discussion with  @Gian  and  @Jeriel  (see meeting minutes)
- [x] [Task]  @Jeriel  Support Operating Model - update diagrams to illustrate product teams and use of RemedyForce and Confluence.
- [x] [Task]  @Jeriel  Cloud Adoption Framework - working on slides (Update: Discussed current progress with Beeps on Google, Amazon CAFs, our current directions, the SOM submitted and discussed with  @Gian  and next steps to refining the slides — need to create a triad between Agile, Technology and DevOps with lessons from prior CAFs.)

## Administrative

Team mostly on leave today (@Kent, @Salia) and handled LWOPs with Renz

