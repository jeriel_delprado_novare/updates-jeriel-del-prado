#!/usr/bin/env bash

# Generates a template.
function generate_template() {
    FILENAME="$1.md"
    FILEDATE=$1

    echo "Generating a new file: ${FILENAME}"

    echo "# ${FILEDATE}" > ${FILENAME}
    echo "" >> ${FILENAME}

    # List your topics here.
    write_heading "${FILENAME}" "DevOps Software Factory"
    write_heading "${FILENAME}" "Administrative"

    echo "Finished generating ${FILENAME}"
}

# Writes a heading with helptext about the section.
function write_heading() {
    FILENAME=$1
    HEADING=$2
    echo "## ${HEADING}" >> ${FILENAME}
    echo "" >> ${FILENAME}
    echo "\`TODO: Enter details about ${HEADING} in this section.\`" >> ${FILENAME}
    echo "" >> ${FILENAME}
    echo "" >> ${FILENAME}

}

#############
### START ###
#############

FILEDATE=$1

# Regardless of where you're executing this script, it should always 
# run where it's located.
cd "$(dirname "$0")"
WORKING=`pwd`

# Perform the following actions
# - Use git to keep things clean in its own branch.
# - Generate the template.
# - Move the results to the `reports` directory.
# - Edit the template.
# - As soon as vim terminates, immediately add the file to git and commit it.
# - Display a message on what had happened.

git checkout master
generate_template "${FILEDATE}"

echo "Preparing to edit the resulting template for the user to fill in..."
mv "${FILEDATE}.md" "reports/"
vim "reports/${FILEDATE}.md"

echo "Finished writing changes to the report. Committing to git..."
git add "reports/${FILEDATE}.md"
git commit -m "Submitted report: ${FILEDATE}"
echo "Done! Report prepared and ready for submission."
echo "-----------------"
echo "To finalize: "
echo "- Review the file if you need to, and the current git log of this new branch."
echo "- If there are changes, use git commit --amend"
echo "- Push the new changes to BitBucket."
echo ""
echo "To revert:"
echo "- Soft reset with git reset --soft HEAD^ then remove the file (or hard reset)"
echo "-----------------"
echo ""
