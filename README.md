# Status Reports for **Jeriel Jan del Prado**

This repository contains my status updates for reporting purposes.

## Usage

Every day, create a Markdown document at `reports/`.

- Set the filename to YYYY-MM-DD.md
- Add a title for the date (# YYYY-MM-DD)
- Add headings for every project
- Describe **Updates for Today**, **Callouts** and **Up Next** sections for each project.

Once completed, commit the new update and push to BitBucket.
